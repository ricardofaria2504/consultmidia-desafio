<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Digital;
use Auth;
use GuzzleHttp\Client;

class DigitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = Digital::orderBy('id','DESC')->paginate(5);
        return view('digital.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('digital.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cpf' => 'required|unique:digitals,cpf',
            'nome' => 'required',
            'dt_nascimento' => 'required',
            'tipo_rede_social' => 'required',
            'usuario' => 'required',
        ]);

        $input = $request->all();

        if ($input->tipo_rede_social == 'instagram') {
            $curl = curl_init();
            curl_setopt_array($curl, [
            CURLOPT_URL => "https://www.instagram.com/{$input->usuario}/channel/?__a=1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_USERAGENT => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.61 Safari/537.36",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => ""
            ]);

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return redirect()->route('digitais.index')->with('success',"cURL Error #:" . $err);
            } else {
                $data = json_decode( $response, true );
                $foto_url = $data['graphql']['user']['profile_pic_url_hd'];
                $seguidores = $data['graphql']['user']['edge_followed_by']['count'];
                $input['foto'] = $foto_url;
                $input['seguidores'] = $seguidores;
            }
        }

        $digital = Digital::create($input);

        return redirect()->route('digitais.index')->with('success','Digital criado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Digital  $digital
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $digital = Digital::find($id);
        return view('digital.show',compact('digital'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Digital  $digital
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $digital = Digital::find($id);
        return view('digital.edit',compact('digital'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Digital  $digital
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cpf' => 'required|unique:digitals,cpf,'.$id,
            'nome' => 'required',
            'dt_nascimento' => 'required',
            'tipo_rede_social' => 'required',
            'usuario' => 'required',
        ]);

        $input = $request->all();

        if ($input['tipo_rede_social'] == 'instagram') {
            $curl = curl_init();
            curl_setopt_array($curl, [
            CURLOPT_URL => "https://www.instagram.com/{$input['usuario']}/channel/?__a=1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_USERAGENT => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.61 Safari/537.36",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => ""
            ]);

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return redirect()->route('digitais.index')->with('danger',"cURL Error #:" . $err);
            } else {
                $data = json_decode( $response, true );
                if ($data == null) {
                    return redirect()->route('digitais.index')->with('danger',"Erro obtendo dados, tente novamente mais tarde.");
                }else{
                    $foto_url = $data['graphql']['user']['profile_pic_url_hd'];
                    $seguidores = $data['graphql']['user']['edge_followed_by']['count'];
                    $input['foto'] = $foto_url;
                    $input['seguidores'] = $seguidores;
                }
            }

        }

        $digital = Digital::find($id);
        $digital->update($input);

        return redirect()->route('digitais.index')->with('success','Digital atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Digital  $digital
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Digital::find($id)->delete();
        return redirect()->route('digitais.index')->with('success','Digital excluído com sucesso');
    }
}
