# ConsultMidia Desafo

Desafio - ConsultMidia - Utilizando o conceito de WEB

## Servidor:
PHP 8.1.5 | MySQL 8.0

## Clonar o projeto

```
git clone https://gitlab.com/ricardofaria2504/consultmidia-desafio.git -b main

```

## Projeto
~~~~
cd consultmidia-desafio
composer update
php -r "file_exists('.env') || copy('.env.example', '.env');"
php artisan key:generate
~~~~
Passo 1:
Instalar o docker e docker-compose na sua maquina.

Inicie um servidor de desenvolvimento docker com:
~~~~
 `./vendor/bin/sail up -d`
~~~~

Depois fornecer as credenciais, você precisará executar a partir da linha de comando:
~~~~
./vendor/bin/sail artisan migrate
~~~~

## URL com Docker
http://localhost 

Caso não use Docker, você também deve adicionar suas informações de banco de dados em arquivo .env:
~~~~ 
BD: 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=consultmidia_desafio
DB_USERNAME=###
DB_PASSWORD=###
~~~~ 

Depois de criar seu banco de dados e fornecer as credenciais, você precisará executar a partir da linha de comando:
~~~~
php artisan migrate
~~~~

Inicie um servidor de desenvolvimento local com `php artisan serve`

## URL com Laravel
http://localhost:8000 
