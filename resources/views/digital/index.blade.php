@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Gerenciamento Digital</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('digitais.create') }}"> Criar novo digital</a>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

@if ($message = Session::get('danger'))
<div class="alert alert-danger">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
 <tr>
   <th>No</th>
   <th>CPF</th>
   <th>Nome</th>
   <th>Data de Nascimento</th>
   <th>Tipo de Rede Social</th>
   <th>Perfil</th>
   <th>Seguidores</th>
   <th>URL</th>
   <th width="280px">Ação</th>
 </tr>
 @foreach ($data as $key => $digital)
  <tr>
    <td>{{ ++$i }}</td>
    <td>{{ $digital->cpf }}</td>
    <td>{{ $digital->nome }}</td>
    <td>{{ \Carbon\Carbon::parse($digital->dt_nascimento)->format('d/m/Y') }}</td>
    <td>{{ $digital->tipo_rede_social }}</td>
    <td><img src="{{ $digital->foto }}"></td>
    <td>{{ $digital->seguidores }}</td>
    <td><a href="https://www.{{$digital->tipo_rede_social}}.com/{{ $digital->usuario }}">Link</a></td>
    <td>
       <a class="btn btn-info" href="{{ route('digitais.show',$digital->id) }}">Mostrar</a>
       <a class="btn btn-primary" href="{{ route('digitais.edit',$digital->id) }}">Editar</a>
       {!! Form::open(['method' => 'DELETE','route' => ['digitais.destroy', $digital->id],'style'=>'display:inline']) !!}
           {!! Form::submit('Deletar', ['class' => 'btn btn-danger']) !!}
       {!! Form::close() !!}
    </td>
  </tr>
 @endforeach
</table>

{!! $data->withQueryString()->links('pagination::bootstrap-5') !!}

@endsection
